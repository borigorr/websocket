<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.3/socket.io.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <style>
        #container-message {
            min-height: 500px;
            border-radius: 5px;
            padding: 15px;
            border: 1px solid #000;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="col" id="container-message">

    </div>
    <form action="" id="form">
        <div class="row">
            <div class="col">
                <div class="form-group">
                    <input type="text"  id="message" class="form-control">
                </div>
            </div>
        </div>
    </form>
</div>
<script>
    var socket = io('http://localhost:5000');
    socket.on('newMessage', function(data) {
        var div = document.createElement('div');
        div.className = "alert alert-primary";
        div.innerHTML = data;
        document.getElementById('container-message').append(div);
    });
    document.getElementById('form').onsubmit = function () {
        var message = document.getElementById('message').value;
        document.getElementById('message').value = "";
        var data = new FormData();
        data.append('message', message);
        axios.post("/ajax.php", data);
        return false
    }
</script>
</body>
</html>