const io = require('socket.io')(5000);
const redis = require('redis').createClient({
    host: 'redis',
    port: 6379,
    password: 'eustatos'
});
redis.subscribe('message');
redis.on('message', function(channel, message) {
    // пересылаем сообщение из канала redis в комнату socket.io
    io.emit('newMessage', message);
});